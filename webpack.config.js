const path = require('path')
const webpack = require('webpack')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  devtool: 'sourcemap',
  output: {
    publicPath: '/build/',
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  module: {
    rules: [{ test: /\.tsx?$/, loader: 'ts-loader' }],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    alias: {
      utils$: path.resolve(__dirname, './src/utils/index.ts'),
    },
  },
  devServer: {
    open: true,
    openPage: 'index.html',
    inline: true,
    noInfo: true,
    port: 3000,
  },
}
