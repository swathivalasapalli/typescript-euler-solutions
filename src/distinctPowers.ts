import { power } from './powerDigitSum'

export const distinctElements = (arr: number[]): number[] => {
  const result: number[] = []
  for (const i of arr) {
    if (result.indexOf(arr[i]) === -1) {
      result.push(arr[i])
    }
  }
  return result
}

export const allPowers = (a: number, b: number): number[] => {
  const result = []
  for (let i = a; i <= b; i += 1) {
    for (let j = a; j <= b; j += 1) {
      result.push(power(i, j))
    }
  }
  return result
}

export const distinctPowers = (a: number, b: number): number[] =>
  distinctElements(allPowers(a, b))

export const countDistinctPowers = (a: number, b: number) =>
  distinctPowers(a, b).length
