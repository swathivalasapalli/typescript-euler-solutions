import { isPrime } from './largestPrimeFactors'
export const primesUpToGivenNumber = (n: number): number[] => {
  const result: number[] = []
  for (let i = 2; i <= n; i += 1) {
    if (isPrime(i)) {
      result.push(i)
    }
  }
  return result
}

export const valueAtIndex = (arr: number[], index: number): number => {
  for (let i = 0; i < arr.length; i += 1) {
    if (i === index) {
      return arr[i]
    }
  }
  return -1
}
export const primeNumberAtIndex = (n: number): number => {
  let countofprimes = 0
  let prime = 1
  while (countofprimes < n) {
    prime += 1
    if (isPrime(prime)) {
      countofprimes += 1
    }
  }
  return prime
}
