export const sumOfIndividualDigits = (n: number): number => {
  let sum = 0
  while (n > 0) {
    sum += Math.floor(n % 10)
    n /= 10
  }
  return sum
}

export const factorial = (n: number): number => {
  let fact = 1
  for (let i = 1; i <= n; i += 1) {
    fact *= i
  }
  return fact
}

export const factorialDigitSum = (n: number): number =>
  sumOfIndividualDigits(factorial(n))
