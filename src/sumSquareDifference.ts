export const sumOfSquares = (n: number): number => {
  let sum = 0
  for (let i = 1; i <= n; i += 1) {
    sum += i * i
  }
  return sum
}

export const squaresOfSum = (n: number): number => {
  let sum = 0
  for (let i = 1; i <= n; i += 1) {
    sum += i
  }
  return sum * sum
}

export const sumSquareDifference = (n: number): number =>
  squaresOfSum(n) - sumOfSquares(n)
