export const max = (x: number, y: number): number => {
  if (x > y) {
    return x
  }
  return y
}

export const lcmOfTwoNumbers = (x: number, y: number): number => {
  let m = max(x, y)
  while (true) {
    if (m % x === 0 && m % y === 0) {
      return m
    }
    m += 1
  }
}

export const smallestMultiple = (n: number): number => {
  let min = 1
  for (let i = 1; i <= n; i += 1) {
    min = lcmOfTwoNumbers(min, i)
  }
  return min
}
