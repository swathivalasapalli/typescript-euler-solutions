import { isPrime } from './largestPrimeFactors'

export const sumOfPrimes = (n: number): number => {
  let sum = 0
  for (let i = 2; i <= n; i += 1) {
    if (isPrime(i)) {
      sum += i
    }
  }
  return sum
}
