export const factors = (n: number): number[] => {
  const result: number[] = []
  for (let i = 1; i <= n; i += 1) {
    if (n % i === 0) {
      result.push(i)
    }
  }
  return result
}

export const isPrime = (n: number): boolean => {
  for (let i = 2; i < n; i += 1) {
    if (n % i === 0) {
      return false
    }
  }
  return true
}

export const largest = (arr: number[]): number => {
  let max = arr[0]
  for (let i = 1; i <= arr.length; i += 1) {
    if (arr[i] > max) {
      max = arr[i]
    }
  }
  return max
}

export const primeFactors = (n: number): number[] => {
  const result: number[] = []
  for (let i = 2; i <= n; i += 1) {
    if (n % i === 0 && isPrime(i)) {
      result.push(i)
    }
  }
  return result
}

export const largestPrimeFactor = (n: number): number =>
  largest(primeFactors(n))
