import { factorial, sumOfIndividualDigits } from './factoriaDigitSum'
export const sumOfCuriousNumber = (n: number): number => {
  let sum = 0
  while (n > 0) {
    sum += factorial(n % 10)
    n = Math.floor(n / 10)
  }
  return sum
}

export const isCuriousNumber = (n: number): boolean =>
  sumOfCuriousNumber(n) === n

export const curiousNumbers = (n: number): number[] => {
  const result: number[] = []
  for (let i = 0; i <= n; i += 1) {
    if (isCuriousNumber(i)) {
      result.push(i)
    }
  }
  return result
}
