import { map, pipe } from 'utils'
import { sumOfMultiples } from './sumOfMultiples'
import {
  fibonacciSeries,
  sumOfEvenNumbers,
  sumOfEvenFibonacciNumbers,
} from './evenFibonacciNumbers'
import {
  factors,
  isPrime,
  largest,
  primeFactors,
  largestPrimeFactor,
} from './largestPrimeFactors'
import { max, lcmOfTwoNumbers, smallestMultiple } from './smallestMultiples'
import {
  sumOfSquares,
  squaresOfSum,
  sumSquareDifference,
} from './sumSquareDifference'
import { sumOfPrimes } from './summationOfPrimes'
import {
  primesUpToGivenNumber,
  valueAtIndex,
  primeNumberAtIndex,
} from './findPrimeAtIndex'
import {
  factorial,
  sumOfIndividualDigits,
  factorialDigitSum,
} from './factoriaDigitSum'

import { power, powerDigitSum } from './powerDigitSum'
import { distinctElements, allPowers, distinctPowers } from './distinctPowers'
import {
  sumOfCuriousNumber,
  isCuriousNumber,
  curiousNumbers,
} from './digitFactorials'

import { countDigits } from './circularPrimeNumbers'
console.log(
  'squares: ',
  pipe(
    [1, 2, 3, 4],
    map(x => x * x),
  ),
)

for (const e of [1, 2, 3, 4]) {
  console.log(e)
}

console.log(sumOfMultiples(10))
console.log(fibonacciSeries(10))
console.log(sumOfEvenNumbers([1, 2, 4, 8]))
console.log(sumOfEvenFibonacciNumbers(10))
console.log(factors(6))
console.log(isPrime(2))
console.log(isPrime(6))
console.log(largest([1, 2, 4, 3, 10, 15, 99, 7]))
console.log(primeFactors(6))
console.log(largestPrimeFactor(10))
console.log(max(100, 3))
console.log(lcmOfTwoNumbers(12, 24))
console.log(lcmOfTwoNumbers(3, 5))
console.log(smallestMultiple(10))
console.log(sumOfSquares(10))
console.log(squaresOfSum(10))
console.log(sumSquareDifference(10))
console.log(sumOfPrimes(10))
console.log(primesUpToGivenNumber(15))
console.log(valueAtIndex([1, 2, 3, 4, 5], 2))
console.log(valueAtIndex(primesUpToGivenNumber(15), 5))
console.log(primeNumberAtIndex(6))
console.log(factorial(5))
console.log(sumOfIndividualDigits(123))
console.log(factorialDigitSum(10))
console.log(power(2, 3))
console.log(powerDigitSum(2, 15))
console.log(distinctElements([1, 3, 2, 4, 1, 5, 3, 3, 8]))
console.log(allPowers(2, 5))
console.log(distinctPowers(2, 5))
console.log(sumOfCuriousNumber(145))
console.log(isCuriousNumber(145))
console.log(isCuriousNumber(144))
console.log(curiousNumbers(150))
console.log(countDigits(12))
