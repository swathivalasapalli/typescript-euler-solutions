import { sumOfIndividualDigits } from './factoriaDigitSum'

export const power = (x: number, n: number): number => {
  let p = 1
  for (let i = 0; i < n; i += 1) {
    p *= x
  }
  return p
}
export const powerDigitSum = (x: number, n: number) =>
  sumOfIndividualDigits(power(x, n))
