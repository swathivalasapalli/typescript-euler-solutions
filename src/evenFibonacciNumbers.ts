export const fibonacciSeries = (n: number): number[] => {
  const result: number[] = [1, 2]
  for (let i = 2; i < n; i += 1) {
    result.push(result[i - 1] + result[i - 2])
  }
  return result
}

export const sumOfEvenNumbers = (arr: number[]): number => {
  let sum = 0
  for (const i of arr) {
    if (i % 2 === 0) {
      sum += i
    }
  }
  return sum
}

export const sumOfEvenFibonacciNumbers = (n: number): number =>
  sumOfEvenNumbers(fibonacciSeries(n))
